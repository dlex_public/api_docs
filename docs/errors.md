<h1><b>Errors</b></h1>

In case of problems with request processing API will return response with status code that represents the error reason. In response body will provided model with details about error.

In case of errors that don't need details or details can't be provided response body will be empty. For example, responses with status code `404` or `500`.

Error structure:

```json
{
  "error": {
    "code": 1001,
    "message": "Details"
  }
}
```

+ `code` - detalize error type from status.
+ `message` - *optional*, contains error description.

General error codes:

+ `1001` - validation error
