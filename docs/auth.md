<h1><b>Authorization</b></h1>

Two keys is used for auth:

  + `access_key` - is used to identify user
  + `secret_key` - is used for signing requests

<h2>Request parameters</h2>

Each request that requires auth should contain such parameters:

  + `access_key`
  + `tonce` - current timestamp in milliseconds. In case of sending two request with same `tonce` second request will fail. `tonce` should not be older than 30 seconds from current server time.
  + `signature` - `hex` representation of request signature.

This params can be passed both in body or in query parameters. In case of sending params in body it should be included in request model:

```json
{
  "access_key": "32lj42l3j4o23",
  "tonce": 123324234,
  "signature": "a1b2b12bab12ab",

  "model_field": "test_field"
}
```

<h2>Generating signature</h2>

Signature is generated the following way:

```python
HEX(HMAC-SHA256( "HTTP-verb|URI|params", secret_key))
```

+ `HTTP-verb` - GET | POST | PUT | PATCH | DELETE
+ `URI` - request path without domain part (exchange.com:3000)
+ `params` - sorted request params, including `access_key` and `tonce`, excluding `signature`. All params included into request should be included into signing-message. So server can check the validity of all params.
+ `secret_key` - secret key

Example:

```python
HEX(HMAC-SHA256(
    "GET|/api/v2/trades/my|access_key=dV6vEJe1CO&market=btcuah&tonce=1465850766246",
    "AYifzxC3Xo"
  ))
signature = 33a694498a2a70cb4ca9a7e28224321e20b41f10217604e9de80ff4ee8cf310e
```

<h2>Errors</h2>

Possible error codes:

+ `401`
    - `2001` - General auth error
    - `2005` - Incorrect signature
    - `2006` - Tonce already used
    - `2007` - Tonce doesn't fit time bounds
    - `2009` - Permission denied
    - `2010` - The access key has expired. Doesn't work for service tokens
    - `2011` - Requested API is out of access key scopes
    - `2012` - Access key does not exists

Error details will be provided in response body.
