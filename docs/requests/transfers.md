<h1><b>Transfers</b></h1>

Transfer means to send assets between user exchange accounts

<h2>Model</h2>

```json
{
  "uuid": "4fefa9e8b78a-43f7-b39e-faa08c4f2224",
  "amount": "1.0",
  "asset": "btc",
  "fee": "0.1",
  "sender_id": 1,
  "recipient_id": 2,
  "created_at": "2018-10-27T16:36:42.000Z",
  "type": "send",
  "recipient_pays_fee": true,
  "balance_change": "-1.0",
  "sender": {
    "uuid": "86e781c0c6bb-42a5-85df-e6eedce6b215",
    "email": "the.admin@dlex.io"
  },
  "recipient": {
    "uuid": "0e4ab5ae3451-4d18-aa08-33a30ee594ea",
    "email": "user@dlex.io"
  }
}
```

where

- `recipient_pays_fee` - if `true` recipient pays fee
- `type` - `send` or `receive`. Based on user who makes request.
- `balance_change` - specifies how transaction changed balance of current user. Based on user who makes request.

<h2>Requests</h2>

<h3>Get list</h3>

Type: `GET`

Endpoint: `api/v1/transfers`

Pagination: `true`

Auth: Registered user

Params:

- `asset` - asset code to filter values.
- `type` - `send` or `receive`.
- `members` - `true` or `false`. Specifies if request should include details about sender and recipient. If `false` specified `sender` and `recipient` fields will be excluded, but request will be faster. **Default: `true`**

<h3>Get transfer</h3>

Type: `GET`

Endpoint: `api/v1/transfers/{uuid}`

Auth: Registered user

Params:
- `members` - `true` or `false`. Specifies if request should include details about sender and recipient. If `false` specified `sender` and `recipient` fields will be excluded, but request will be faster. **Default: `true`**


<h3>Create transfer</h3>

Type: `POST`

Endpoint: `api/v1/transfers`

Auth: Registered user

Body:

```json
{
  "asset": "btc",
  "amount": "10.0",
  "recipient_pays_fee": true,
  "recipient_email": "some@user.com" # Or "recipient_id": 1
}
```

where:

- `asset` - **Required**
- `amount` - **Required**
- `recipient_pays_fee` - if `true` recipient pays fee. **Default: `false`**
- `recipient_email` - Specifies the recipient of transfer by email.
- `recipient_id` - Specifies recipient by id.

*Note: only one of fields `recipient_email` or `recipient_id` should be specified*
