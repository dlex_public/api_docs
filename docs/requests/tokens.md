<h1><b>API tokens</b></h1>

<h2>Model</h2>

```json
{
  "id": 3,
  "label": "Some label",
  "access_key": "7xExr8cX0P69OCQ32x8DBHOnzCZ3xD9Sbjypu6dU",
  "secret_key": "Gz710qWn2PxCb8IfLgu5N1o3SxTfu0joLWsTDm14",
  "expire_at": "2029-12-31T22:00:00.000Z",
  "tag_list": [
    "read",
    "trade",
    "withdraw"
  ],
  "created_at": "2018-10-09T11:58:43.000Z",
  "updated_at": "2018-10-09T11:58:43.000Z"
}
```

where

- `tag_list` - list of tags that determines what actions are available for this token. Possible values: `read` - read profile info and account history, `trade` - perform trading operations, `withdraw` - perform withdraw.

<h2>Requests</h2>

<h3>Get list</h3>

Type: `GET`

Endpoint: `api/v1/tokens`

Pagination: `true`

Auth: Registered user

<h3>Get token</h3>

Type: `GET`

Endpoint: `api/v1/tokens/:id`

Auth: Registered user


<h3>Create token</h3>

Type: `POST`

Endpoint: `api/v1/tokens`

Auth: Registered user

OTP: `true`

Request body:

```json
{
  "label": "Label",
  "expire_at": "01.01.2030",
  "tag_list": "read|trade|withdraw"
}
```

where

- `label` - some string title for token.
- `expire_at` - token expiration time. Default is 1 year from token creation.
- `tag_list` - available actions divided by `|`. By default all actions will be enabled.

<h3>Update token</h3>

Type: `PATCH`

Endpoint: `api/v1/tokens/:id`

Auth: Registered user

OTP: `true`

Request body:

```json
{
  "label": "Label",
  "expire_at": "01.01.2030",
  "tag_list": "read|trade|withdraw"
}
```

*Only presented params will be changed. At list one of params should be presented.*

<h3>Delete token</h3>

Type: `DELETE`

Endpoint: `api/v1/tokens/:id`

Auth: Registered user

OTP: `true`
