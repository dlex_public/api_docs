<h1><b>Registration</b></h1>

<h2>Requests</h2>

<h3>Register new user</h3>

Type: `POST`

Endpoint: `api/v1/registration`

Body:

```json
{
    "email": "test@mail.com",
    "password": "some_pass",
    "invite": "sdf213"
}
```

where:

- `email` - **Required**
- `password` - **Required**
- `invite` - **Optional**. Invitation code if it is enabled in configs

<h3>Login</h3>

Type: `POST`

Endpoint: `api/v1/login`

Body:

```json
{
    "email": "test@mail.com",
    "password": "some_pass"
}
```

where:

- `email` - **Required**
- `password` - **Required**

Result:

```json
{
    "access_key": "21XljYPGFxLyeUxhfHV3BxxPaJbJ8FvB2x528FND",
    "secret_key": "Avmf4TJYx5jaSbSu6bMqImnJCJ5YEDnJnmxxxkNq",
    "expire_at": "2020-05-20T16:07:27.303Z",
    "tag_list": [
        "read",
        "trade",
        "withdraw"
    ]
}
```
