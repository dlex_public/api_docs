<h1><b>Transactions</b></h1>

<h2>Model</h2>

```json
{
  "uuid": "d939c88a1b8b-4e10-b7bf-83ad3095477f",
  "asset": "btc",
  "amount": "10.0",
  "fee": "0.0",
  "status": "processing",
  "options": {
    "type": "crypto",
    "data": {
      "tx_id": "fdfbf222888edc45ac37576a52b05c406a411ca1d26f936ea6854e0908b581ac",
      "counterparty_address": "1MkMrYCidKhNJjWpHiqrZPTHjih5mdWdn2"
    }
  },
  "done_at": null,
  "updated_at": "2018-10-08T11:08:42.000Z",
  "created_at": "2018-10-08T11:08:42.000Z",
  "type": "deposit"
}
```

where

- `options` - is additional details. Details is placed in `data` field and details structure depends on `type` field.

  Possible values:

  Type: `crypto`

```json
{
  "tx_id": "fdfbf222888edc45ac37576a52b05c406a411ca1d26f936ea6854e0908b581ac",
  "counterparty_address": "1MkMrYCidKhNJjWpHiqrZPTHjih5mdWdn2"
}
```

<h2>Requests</h2>

<h3>Get list</h3>

Type: `GET`

Endpoint: `api/v1/transactions`

Pagination: `true`

Auth: Registered user

Params:

- `asset` - asset code to filter values.
- `type` - `deposit` or `withdrawal`.

<h3>Get transaction</h3>

Type: `GET`

Endpoint: `api/v1/transactions/{uuid}`

Auth: Registered user
