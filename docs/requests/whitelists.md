<h1><b>Whitelisted items</b></h2>

Whitelist represents model for storing withdraw destinations that don't require additional confirmation by user while withdrawing. For example, entering 2FA code.

<h2>Model</h2>

```json
{
  "id": 1,
  "asset": "btc",
  "title": "My test",
  "destination": "n3xH9RHpVBDNvujfy62vuwjgsZ8Gwc4wcD"
}
```

<h2>Requests</h2>

<h3>Get whitelisted items list</h3>

Type: `GET`

Endpoint: `api/v1/whitelists`

Pagination: `true`

Auth: Registered user

Params:

- `asset` - asset code to filter values.

<h3>Get whitelisted item</h3>

Type: `GET`

Endpoint: `api/v1/whitelists/{id}`

Auth: Registered user

<h3>Create whitelisted item</h3>

Type: `POST`

Endpoint: `api/v1/whitelists`

Auth: Registered user

Body:

```json
{
  "asset": "btc",
  "title": "Some title",
  "destination": "n3xH9RHpVBDNvujfy62vuwjgsZ8Gwc4wcD"
}
```

where:

- `asset` - **Required**
- `destination` - **Required**

<h3>Update whitelisted item</h3>

Type: `PATCH`

Endpoint: `api/v1/whitelists/{id}`

Auth: Registered user

Body:

```json
{
  "title": "Some title",
  "destination": "n3xH9RHpVBDNvujfy62vuwjgsZ8Gwc4wcD"
}
```

<h3>Delete whitelisted item</h3>

Type: `DELETE`

Endpoint: `api/v1/whitelists/{id}`

Auth: Registered user
