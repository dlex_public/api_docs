<h1><b>Tickers</b></h1>

All requests do not require auth

<h2>Model</h2>

```json
{
  "at": 1548256108,
  "name": "ETH/BTC",
  "base_unit": "eth",
  "quote_unit": "btc",
  "ticker": {
    "buy": "0.0321",
    "sell": "0.0322",
    "low": "0.0313",
    "high": "0.0346",
    "last": "0.0321",
    "open": "0.0379",
    "vol": "782.179457",
    "price": "25.8517085681"
  }
}
```

+ `last` - price of last trade
+ `vol` - volume per last 24 hours
+ `price` - price per last 24 hours - volume * average_price

<h2>Requests</h2>

<h3>Get tickers</h3>

Returns dictionary of all tickers

Type: `GET`

Endpoint: `api/v1/tickers`

Response:

```json
{
  "ethbtc": {
    "at": 1548256108,
    "name": "ETH/BTC",
    "base_unit": "eth",
    "quote_unit": "btc",
    "ticker": {
      "buy": "0.0321",
      "sell": "0.0322",
      "low": "0.0313",
      "high": "0.0346",
      "last": "0.0321",
      "open": "0.0379",
      "vol": "782.179457",
      "price": "25.8517085681"
    }
  }
}
```

<h3>Get tickers for market</h3>

Returns ticker for specific market.

Type: `GET`

Endpoint: `api/v1/tickers/{market_code}`
